package com.songminju.jdagger;

/*
@User mejee song
@Date 2017/9/12 
@Description 检查工具类，其中NsNull代表为null或者值为空白
*/
public class JDCheck {
    /**
    *@author song(mejeesong@qq.com)
    *@date 2018年3月10日
    *@param objects
    *@return 参数是否有NsNull
    *
    */
    public static boolean hasNsNull(Object ...objects){
        for(Object obj:objects){
            if(obj ==null || "".equals(obj)){
                return true;
            }
        }
        return false;
    }
    public static boolean allNsNull(Object ...objects) {
        for(Object obj:objects) {
            if(obj != null && !obj.equals("")) {
                return false;
            }
        }
        return true;
    }
    public static boolean hasNull(Object ...objects){
        for(Object obj:objects){
            if(obj ==null){
                return true;
            }
        }
        return false;
    }
    public static boolean allNull(Object ...objects) {
        for(Object obj:objects) {
            if(obj != null) {
                return false;
            }
        }
        return true;
    }
}
