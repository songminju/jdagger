package com.songminju.jdagger;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

public class JDConfiguration {
    public static Map<String,String> loadPropertiesFile(String file){
        Map<String,String> data = new HashMap<>();
        Properties prop = new Properties();
        try {
            prop.load(JDConfiguration.class.getClassLoader().getResourceAsStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Set<Object> keys = prop.keySet();
        for(Object key:keys){
            String keyStr = Objects.toString(key);
            String value = prop.getProperty(keyStr);
            data.put(keyStr,value);
        }
        return data;
    }
}
