package com.songminju.jdagger;

import java.util.regex.Pattern;

/*
@User mejee song
@Date 2017/9/12 
@Description 
*/
public class JDString {
	
	/**
	*@author song(mejeesong@qq.com)
	*2018年3月10日
	*一些常见的正则表达式,_86结尾的都是指代中国大陆地区专用.
	*/
	public final static class RegexStr{
		public final static String MOBILE_PHONE_86 = "";
		public final static String POSTAL_CODE_86="";
		public final static String ID_NUMBER_86 = "";
		
		
		public final static String DOMAIN = "";
		public final static String QQ_NUMBER = "";
		public final static String EMAIL = "";
		public final static String NUMBER = "";
		public final static String URL = "";
		public final static String CHINEXE = "^[\\u4e00-\\u9fa5]{0,}$";
		public final static String IPV4 = "";
		public final static String IPV6 = "";
		
	}
	
	public boolean match(CharSequence content,String regex) {
		return Pattern.matches(regex, content);
	}
	
}
