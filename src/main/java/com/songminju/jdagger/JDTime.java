package com.songminju.jdagger;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
@User mejee song
@Date 2017/9/12
@Description 
*/
public class JDTime {

    public static String strOfNow(){
        return strOf(new Date());
    }
    public static String strOfNow(String pattern){
        return strOf(new Date(),pattern);
    }

    public static String strOf(Date date){
        return strOf(date,"YYYY-MM-DD HH:mm:ss");
    }
    
    public static String strOf(Date date,String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

}
