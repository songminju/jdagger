package com.songminju.jdagger;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author song(mejeesong@qq.com) 2018年1月25日
 *
 */
public class JDEncode {
	
	private static final char[] hexs = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	
	public static String md5(String str) {
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			return toHex(md5.digest(str.getBytes("utf-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String toHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for(byte b:bytes) {
			sb.append(hexs[(b>>4)& 0x0f]);
			sb.append(hexs[b & 0x0f]);
		}
		return sb.toString();
	}
}
